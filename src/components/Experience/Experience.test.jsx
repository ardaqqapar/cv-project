import React from 'react';
import { render, screen } from '@testing-library/react';
import Experience from './Experience';

const mockData = [
  {
    id: 1,
    company: 'Company A',
    job: 'Job A',
    date: 'January 2020 - Present',
    description: 'Lorem ipsum dolor sit amet',
  },
  {
    id: 2,
    company: 'Company B',
    job: 'Job B',
    date: 'February 2018 - December 2019',
    description: 'Consectetur adipiscing elit',
  },
];

describe('Experience component', () => {
  test('renders job company names', () => {
    render(<Experience data={mockData} />);
    const companyNames = mockData.map((job) =>
      screen.getByTestId(`company-${job.id}`)
    );
    expect(companyNames).toHaveLength(mockData.length);
  });

  test('renders job positions', () => {
    render(<Experience data={mockData} />);
    const jobPositions = mockData.map((job) =>
      screen.getByTestId(`position-${job.id}`)
    );
    expect(jobPositions).toHaveLength(mockData.length);
  });

  test('renders job dates', () => {
    render(<Experience data={mockData} />);
    const jobDates = mockData.map((job) =>
      screen.getByTestId(`date-${job.id}`)
    );
    expect(jobDates).toHaveLength(mockData.length);
  });

  test('renders job descriptions', () => {
    render(<Experience data={mockData} />);
    const jobDescriptions = mockData.map((job) =>
      screen.getByTestId(`description-${job.id}`)
    );
    expect(jobDescriptions).toHaveLength(mockData.length);
  });
});

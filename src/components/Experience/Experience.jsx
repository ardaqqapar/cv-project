import './Experience.scss'

import React from 'react'

const Experience = ({ data }) => {
    return (
      <div className="experience">
        {data.map((job) => (
          <div className="wrapper" key={job.id}>
            <div className="company" data-testid={`company-${job.id}`}>
              {job.company}
            </div>
            <div className="position" data-testid={`position-${job.id}`}>
              {job.job}
            </div>
            <div className="date-of-work" data-testid={`date-${job.id}`}>
              {job.date}
            </div>
            <div className="description" data-testid={`description-${job.id}`}>
              {job.description}
            </div>
          </div>
        ))}
      </div>
    );
  };

export default Experience
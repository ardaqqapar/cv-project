import './PortfolioInfo.scss'

import React from 'react'

const PortfolioInfo = ({title, text, url, type}) => {
    return (
        <div className='portfolioInfo'>
            <h4>{title}</h4>
            <p>{text}</p>
            <a href={url}>View source</a>
        </div>
    )
}

export default PortfolioInfo
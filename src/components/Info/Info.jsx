import './Info.scss'

import React from 'react'

const Info = ({text}) => {
    return(
        <p className='info-box'>
            {text}
        </p>
    )
} 

export default Info
import { HashLink } from 'react-router-hash-link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faGraduationCap, faPen, faSuitcase, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { faComment, faGem } from '@fortawesome/free-regular-svg-icons'
import React from 'react';

import './Navigation.scss'

const menuItems = [
    {
        icon: faUser,
        id: '#aboutme',
        text: 'About Me'
    },
    {
        icon: faGraduationCap,
        id: '#education',
        text: 'Education'
    },
    {
        icon: faPen,
        id: '#experience',
        text: 'Experience'
    },
    {
        icon: faGem,
        id: '#skills',
        text: 'Skills'
    },
    {
        icon: faSuitcase,
        id: '#portfolio',
        text: 'Portfolio'
    },
    {
        icon: faPaperPlane,
        id: '#contacts',
        text: 'Contacts'
    },
    {
        icon: faComment,
        id: '#feedbacks',
        text: 'Feedbacks'
    }
]

const Navigation = () => {
    return (
        <div className="navigation" data-testid="navigation">
            {menuItems.map((item)=> (
                <HashLink to={item.id} key={item.id} smooth>
                    <div className='item' >
                        <div className="icon">
                            <FontAwesomeIcon icon={item.icon}/> 
                        </div>
                        <div className="link">
                            {item.text}
                        </div>
                    </div>
                </HashLink>
            ))}
        </div>
    )
}

export default Navigation
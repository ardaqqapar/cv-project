import React from 'react';
import { render, screen } from '@testing-library/react';
import Navigation from './Navigation';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';


const mockMenuItems = [
  {
    icon: 'faUser',
    id: '#aboutme',
    text: 'About Me',
  },
  {
    icon: 'faGraduationCap',
    id: '#education',
    text: 'Education',
  },
  {
    icon: 'faPen',
    id: '#experience',
    text: 'Experience',
  },
  {
    icon: 'faGem',
    id: '#skills',
    text: 'Skills',
  },
  {
    icon: 'faSuitcase',
    id: '#portfolio',
    text: 'Portfolio',
  },
  {
    icon: 'faPaperPlane',
    id: '#contacts',
    text: 'Contacts',
  },
  {
    icon: 'faComment',
    id: '#feedbacks',
    text: 'Feedbacks',
  },
];

describe('Navigation component', () => {
  test('renders correct number of menu items', () => {
    render(<BrowserRouter>
        <Navigation />
      </BrowserRouter>);
    const navigationItems = screen.getAllByRole('link');
    expect(navigationItems).toHaveLength(mockMenuItems.length);
  });

  

  test('renders menu item text', () => {
    render(<BrowserRouter>
        <Navigation />
      </BrowserRouter>);
    const menuTexts = mockMenuItems.map((item) =>
      screen.getByText(item.text)
    );
    expect(menuTexts).toHaveLength(mockMenuItems.length);
  });

});

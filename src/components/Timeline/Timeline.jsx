import { useEffect } from 'react'
import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useDispatch, useSelector } from 'react-redux';
import { fetchEducations, selectEducations, selectLoading, selectError } from './timelineSlice';

import './Timeline.scss'

const Timeline = () => {

    const educations = useSelector(selectEducations);
    const loading = useSelector(selectLoading);
    const error = useSelector(selectError);
    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(fetchEducations());
    }, [dispatch]);

    return (
      <div className="timeline">
        {loading && <div className='loading-container'><FontAwesomeIcon icon={faArrowsRotate} className='loading'/></div>}
        {error && <div className='timeline-error'>Something went wrong; please review your server connection!</div>}
        {educations && (
          <div className="content">
              <div className="green-line"></div>
              <div>
                {educations.map((education) => (
                    <div className="container" key={education.date}>
                      <div className="date">{education.date}</div>
                      <div className="text-box">
                        <h2>{education.title}</h2>
                        <p>{education.text}</p>
                        <span className='pointer-arrow'></span>
                      </div>
                    </div>
                ))}
              </div>
          </div>
        )}
      </div>
    )
}

export default Timeline
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchEducations = createAsyncThunk(
  'timeline/fetchEducations',
  async () => {
    try {
      const response = await axios.get('/api/educations');
      return response.data.educations;
    } catch (error) {
      throw error;
    }
  }
);

const timelineSlice = createSlice({
  name: 'timeline',
  initialState: {
    educations: [],
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchEducations.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchEducations.fulfilled, (state, action) => {
        state.loading = false;
        state.educations = action.payload;
      })
      .addCase(fetchEducations.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const selectEducations = (state) => state.timeline.educations;
export const selectLoading = (state) => state.timeline.loading;
export const selectError = (state) => state.timeline.error;

export default timelineSlice.reducer;


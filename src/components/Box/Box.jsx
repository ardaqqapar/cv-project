import './Box.scss'
import React from 'react'

const Box = ( {title, id, content} ) => {
    return(
        <div className='box' id={id} data-testid="box">
            <h1>{title}</h1>
            <div>{content}</div>
        </div>
    )
}

export default Box
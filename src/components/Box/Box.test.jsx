import React from 'react';
import { render, screen } from '@testing-library/react';
import Box from './Box';
import '@testing-library/jest-dom/extend-expect';

test('renders box with title and content', () => {
  const title = 'Test Title';
  const id = 'test-box';
  const content = 'Test Content';

  render(<Box title={title} id={id} content={content} />);

  const boxElement = screen.getByTestId('box');
  const titleElement = screen.getByText(title);
  const contentElement = screen.getByText(content);

  expect(boxElement).toBeInTheDocument();
  expect(titleElement).toBeInTheDocument();
  expect(contentElement).toBeInTheDocument();
  expect(boxElement).toHaveAttribute('id', id);
});

import PortfolioInfo from '../PortfolioInfo/PortfolioInfo'
import './Portfolio.scss'

import { useState } from 'react'
import { motion } from 'framer-motion'
import React from 'react'

const Portfolio = ( { portfolioData } ) => {

    const [filter, setFilter] = useState('all')
    const toggle = (filter) => {
        setFilter(filter)
    }

    const filteredData = portfolioData.filter((data) => data.type === filter || filter === 'all');

    return(
        <div className="portfolio" data-testid="portfolio">
            <div className="filter">
                <p className={`filter-tag ${(filter==='all') ? 'selected' : ''}`} onClick={()=>toggle('all')}>All</p>
                <p>/</p>
                <p className={`filter-tag ${(filter==='code') ? 'selected' : ''}`} onClick={()=>toggle('code')}>Code</p>
                <p>/</p>
                <p className={`filter-tag ${(filter==='ui') ? 'selected' : ''}`} onClick={()=>toggle('ui')}>UI</p>
            </div>
            <div className='portfolio-list' data-testid="portfolio-list">
                {filteredData.map((data) => (
                    <motion.div
                        layout
                        initial={{scale: 0.5, opacity:0}}
                        animate={{scale: 1, opacity:1}}
                        exit={{scale:0.5, opacity: 0}}
                        key={data.id}
                        className='card'
                    >
                        <img src={data.card} alt={data.title} />
                        <div className="info">
                        <PortfolioInfo title={data.title} text={data.text} url={data.url} />
                        </div>
                    </motion.div>
                ))}
            </div>
        </div>
    )
}

export default Portfolio
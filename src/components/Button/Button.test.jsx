import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import '@testing-library/jest-dom/extend-expect';


import Button from './Button';

describe('Button component', () => {
  test('renders button text', () => {
    render(<Button text="Click me" />);
    const buttonText = screen.getByText(/Click me/i);
    expect(buttonText).toBeInTheDocument();
  });

  // test('renders button icon', () => {
  //   render(<Button icon={faPenToSquare} />);
  //   const buttonIcon = screen.getByTestId('button-icon');
  //   expect(buttonIcon).toBeInTheDocument();
  // });

  test('calls action on button click', () => {
    const mockAction = jest.fn();
    render(<Button text="Click me" action={mockAction} />);
    const button = screen.getByText(/Click me/i);
    fireEvent.click(button);
    expect(mockAction).toHaveBeenCalledTimes(1);
  });

  // test('disables button when disabled prop is true', () => {
  //   render(<Button text="Click me" disabled />);
  //   const button = screen.getByText(/Click me/i);
  //   expect(button).toBeDisabled();
  // });

  test('renders loading content when loading prop is true', () => {
    render(<Button text="Click me" loading={<div>Loading...</div>} />);
    const loadingText = screen.getByText(/Loading.../i);
    expect(loadingText).toBeInTheDocument();
  });
});

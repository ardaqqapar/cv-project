import './Button.scss'

import { Link } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React from "react"

const Button = ( { icon, text, link, type, disabled, action, loading } ) => {
    return (
        <>
            {link ? <Link to={link}>
                    <button className={icon ? 'button small' : 'button'} data-testid="button" >
                        <FontAwesomeIcon icon={icon}/>
                        <p>{text}</p>
                    </button>
                </Link>
            :
                (<button className={icon ? 'button small' : 'button'} onClick={action} type={type} disabled={disabled} data-testid="button" >
                    {icon && <FontAwesomeIcon icon={icon} />}
                    {text && !loading && <p>{text}</p>}
                    {loading}
                </button>)
            }
        </>
    )
}
export default Button
import './Skills.scss'
import Button from '../Button/Button';

import { useSelector, useDispatch } from 'react-redux';
import React from 'react';
import { useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { faPenToSquare, faArrowsRotate } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fetchSkills, createSkill, selectSkills, selectLoading, selectError, toggleFormVisibility } from './skillsSlice';


const Skills = () => {

    const skills = useSelector(selectSkills);
    const loading = useSelector(selectLoading);
    const isFormVisible = useSelector((state) => state.skills.isFormVisible); // Get isFormVisible from the store

    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(fetchSkills());
    }, [dispatch]);

    const handleSubmit = async (formData) => {
      try {
        dispatch(createSkill(formData));
      } catch (error) {
        console.error('Error creating skill:', error);
      }
    };

    return (
      <div className="skills-container">
        <div className="button-position">
          <Button icon={faPenToSquare} action={()=>dispatch(toggleFormVisibility())} text={isFormVisible ? 'Close edit' : 'Open edit'}/>
        </div>
        {isFormVisible && <Formik
          initialValues={{ name: "", range: "" }}
          validationSchema={Yup.object({
            name: Yup.string().required("Skill name is a required field"),
            range: Yup.number()
              .typeError("Skill range must be a 'number' type")
              .required("Skill range is a required field")
              .min(10, "Skill range must be greater than or equal to 10")
              .max(100, "Skill range must be less than or equal to 100"),
          })}
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
            resetForm();
          }}
          
        >
          {formik => (
            <Form className="skill-form">
              <div className={`form-item ${formik.errors.name && formik.touched.name ? "error" : ""}`}>
                <div className="item-name">Skill name:</div>
                <Field type="text" name="name" placeholder="Enter skill name" required />
              </div>
              <ErrorMessage name="name" component="div" className="error-message" />

              <div className={`form-item ${formik.errors.range && formik.touched.range ? "error" : ""}`}>
                <div className="item-name">Skill range:</div>
                <Field type="number" name="range" placeholder="Enter skill range" required />
              </div>
              <ErrorMessage name="range" component="div" className="error-message" />
            
              <Button 
                type="submit" 
                disabled={!formik.isValid || !formik.dirty} 
                text='Add Skill' 
                loading={loading && <div className='submit-loading'><FontAwesomeIcon icon={faArrowsRotate} className='loading'/></div>}
              />
            </Form>
          )}
        </Formik>}
        {skills.map((skill) => (
          <div className="skill" key={skill.skill.id}>
            <div className="skill-progress">
              <div className="skill-name">{skill.skill.name}</div>
              <div className="progress" style={{  width: `${skill.skill.range}%`}}></div>
            </div>
          </div>
        ))}

        <div className="breakpoints">
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
          <div className="breakpoint"></div>
        </div>
        
        <div className="skill-levels">
            <p>Beginner</p>
            <p>Proficient</p>
            <p>Expert</p>
            <p>Master</p>
        </div>
      </div>
    );
  }

export default Skills

// this solution works properly when I have a proper API with changing data, but for MirageJS is not the case
    // useEffect(() => {
    //   const fetchSkills = async () => {
    //     try {
    //       const response = await axios.get("/api/skills");
    //       setSkills(response.data.skills);
    //     } catch (error) {
    //       console.error("Error fetching skills:", error);
    //     }
    //   };

    //   fetchSkills();

      
    // }, []); 
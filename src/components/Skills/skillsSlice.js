import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

const fetchSkillsFromLocalStorage = () => {
  const skillsFromLocalStorage = JSON.parse(localStorage.getItem('skills'));
  return skillsFromLocalStorage || [];
};

export const fetchSkills = createAsyncThunk('skills/fetchSkills', async () => {
  return fetchSkillsFromLocalStorage();
});

export const createSkill = createAsyncThunk('skills/createSkill', async (formData) => {
    try {
      // Simulate server request delay
      await new Promise((resolve) => setTimeout(resolve, 1000));
  
      const newSkillWithId = {
        ...formData,
        id: uuidv4(),
      };
  
      // Send POST request to the server
      const response = await axios.post('/api/skills', newSkillWithId);
      const createdSkill = response.data;
  
      // Update local storage
      const skillsFromLocalStorage = fetchSkillsFromLocalStorage();
      const updatedSkills = [...skillsFromLocalStorage, createdSkill];
      localStorage.setItem('skills', JSON.stringify(updatedSkills));
  
      return updatedSkills;
    } catch (error) {
      throw new Error('Error creating skill');
    }
  });

const skillsSlice = createSlice({
  name: 'skills',
  initialState: {
    skills: [],
    loading: false,
    error: null,
    isFormVisible: false,
  },
  reducers: {
    toggleFormVisibility: (state) => {
        state.isFormVisible = !state.isFormVisible;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchSkills.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.skills = action.payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(fetchSkills.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(createSkill.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(createSkill.fulfilled, (state, action) => {
        state.skills = action.payload;
        state.loading = false;
        state.error = null;
      })
      .addCase(createSkill.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const selectSkills = (state) => state.skills.skills;
export const selectLoading = (state) => state.skills.loading;
export const selectError = (state) => state.skills.error;
export const { toggleFormVisibility } = skillsSlice.actions;

export default skillsSlice.reducer;

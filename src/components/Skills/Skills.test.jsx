import React from 'react';
import { render, fireEvent, screen, act } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import '@testing-library/jest-dom/extend-expect';

import Skills from './Skills';

// Mock the necessary Redux store and actions

test('renders the Skills component', () => {
  act(() => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>
    );
  });

  // Add your assertions here
});

test('toggles form visibility on button click', () => {
  act(() => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>
    );
  });

  const openEditButton = screen.getByText('Open edit');
  act(() => {
    fireEvent.click(openEditButton);
  });

  expect(screen.getByText('Close edit')).toBeInTheDocument();

  act(() => {
    fireEvent.click(openEditButton);
  });

  expect(screen.getByText('Open edit')).toBeInTheDocument();
});

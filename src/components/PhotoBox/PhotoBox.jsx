import avatar from '../../assets/avatar.png'
import './PhotoBox.scss'

import React from 'react'

const PhotoBox = ( {name, title, description, type } ) => {
    return (
        <div className={`photoBox ${type}`} data-testid="photo-box">
            <img src={avatar}/>
            <h1>{name}</h1>
            <h2>{title}</h2>
            <p>{description}</p>
        </div>
    )
}

export default PhotoBox
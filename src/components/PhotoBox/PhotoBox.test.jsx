import React from 'react';
import { render } from '@testing-library/react';
import PhotoBox from './PhotoBox';
import '@testing-library/jest-dom/extend-expect';


jest.mock('../../assets/avatar.png', () => '../../assets/avatar.png');


describe('PhotoBox', () => {
  const mockData = {
    name: 'John Doe',
    title: 'Developer',
    description: 'Lorem ipsum dolor sit amet',
    type: 'small',
  };

  test('renders with correct data', () => {
    const { getByTestId, getByText } = render(
      <PhotoBox {...mockData} />
    );

    const photoBox = getByTestId('photo-box');
    const nameElement = getByText(mockData.name);
    const titleElement = getByText(mockData.title);
    const descriptionElement = getByText(mockData.description);

    expect(photoBox).toBeInTheDocument();
    expect(photoBox).toHaveClass(mockData.type);
    expect(nameElement).toBeInTheDocument();
    expect(titleElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
  });

  // Add more tests if needed
});

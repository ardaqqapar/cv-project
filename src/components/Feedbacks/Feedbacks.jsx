import Info from "../Info/Info"
import './Feedbacks.scss'

import React from 'react';

const Feedbacks = ( {data} ) => {
    return (
        <div className="feedbacks">
            {data.map((entry)=>(
                <div className="feedback" key={entry.id}>
                    <Info text={entry.feedback}/>
                    <div className="reporter">
                        <img src={entry.reporter.photoUrl}/>
                        <p>{entry.reporter.name}, <a href={entry.reporter.citeUrl}>{entry.reporter.citeUrl}</a></p>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Feedbacks
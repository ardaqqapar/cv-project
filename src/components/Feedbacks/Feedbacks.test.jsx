import React from 'react';
import { render, screen } from '@testing-library/react';
import Feedbacks from './Feedbacks';
import '@testing-library/jest-dom/extend-expect';


const mockData = [
  {
    id: 1,
    feedback: 'Great service!',
    reporter: {
      name: 'John Doe',
      photoUrl: 'https://example.com/john-doe.jpg',
      citeUrl: 'https://example.com/john-doe',
    },
  },
  {
    id: 2,
    feedback: 'Excellent job!',
    reporter: {
      name: 'Jane Smith',
      photoUrl: 'https://example.com/jane-smith.jpg',
      citeUrl: 'https://example.com/jane-smith',
    },
  },
];

describe('Feedbacks component', () => {
  test('renders feedback texts', () => {
    render(<Feedbacks data={mockData} />);
    const feedbackTexts = mockData.map((entry) =>
      screen.getByText(entry.feedback)
    );
    expect(feedbackTexts).toHaveLength(mockData.length);
  }); 

  test('renders reporter photo URLs', () => {
    render(<Feedbacks data={mockData} />);
    const reporterPhotos = screen.getAllByRole('img');
    const reporterPhotoUrls = mockData.map((entry) => entry.reporter.photoUrl);
    reporterPhotos.forEach((photo, index) => {
      expect(photo).toHaveAttribute('src', reporterPhotoUrls[index]);
    });
  });

  test('renders reporter cite URLs', () => {
    render(<Feedbacks data={mockData} />);
    const reporterCiteLinks = screen.getAllByRole('link');
    const reporterCiteUrls = mockData.map((entry) => entry.reporter.citeUrl);
    reporterCiteLinks.forEach((link, index) => {
      expect(link).toHaveAttribute('href', reporterCiteUrls[index]);
    });
  });
});

import React from 'react';
import { render, screen } from '@testing-library/react';
import Panel from './Panel';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter } from 'react-router-dom';


jest.mock('../../assets/avatar.png', () => '../../assets/avatar.png');

describe('Panel', () => {
  test('renders Panel component', () => {
    render(<BrowserRouter><Panel navCollapse={false} /></BrowserRouter>);
    
    // Assert that the component renders without errors
    expect(screen.getByTestId('panel-component')).toBeInTheDocument();
  });

  test('renders PhotoBox component', () => {
    render(<BrowserRouter><Panel navCollapse={false} /></BrowserRouter>);
    
    // Assert that the PhotoBox component is rendered
    expect(screen.getByTestId('photo-box')).toBeInTheDocument();
  });

  test('renders Navigation component', () => {
    render(<BrowserRouter><Panel navCollapse={false} /></BrowserRouter>);
    
    // Assert that the Navigation component is rendered
    expect(screen.getByTestId('navigation')).toBeInTheDocument();
  });

  test('renders Button component with correct props', () => {
    render(<BrowserRouter><Panel navCollapse={false} /></BrowserRouter>);
    
    // Assert that the Button component is rendered with the correct props
    const button = screen.getByTestId('button');
    expect(button).toBeInTheDocument();
    expect(button).toHaveTextContent('Go back');
    // You can also assert the icon here if required
  });

  test('applies "navCollapse" class when navCollapse prop is true', () => {
    render(<BrowserRouter><Panel navCollapse={true} /></BrowserRouter>);
    
    // Assert that the component has the "navCollapse" class
    expect(screen.getByTestId('panel-component')).toHaveClass('navCollapse');
  });
});

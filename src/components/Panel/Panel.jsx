import PhotoBox from "../PhotoBox/PhotoBox"
import Button from "../Button/Button"
import './Panel.scss'
import Navigation from "../Navigation/Navigation"

import { faChevronLeft } from "@fortawesome/free-solid-svg-icons"
import React from "react"

const Panel = ({navCollapse}) => {
    return (
        <div className={`panel ${navCollapse ? "navCollapse" : ""}`} data-testid="panel-component">
            <PhotoBox className='panelPhotoBox' type='small' name='Ardak Kapar'  />
            <Navigation />
            <div className="buttonSpace">
                <Button text='Go back' link='/' icon={faChevronLeft}/>
            </div>
        </div>
    )
}

export default Panel
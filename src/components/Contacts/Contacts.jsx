import './Contacts.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebookF, faSkype } from '@fortawesome/free-brands-svg-icons'
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import React from 'react'


const contacts = [
    {
        id: 1,
        icon: faPhone,
        contactName: '500 342 242',
        link: "tel:500342342",
        info: null,
    },
    {
        id: 2,
        icon: faEnvelope,
        contactName: 'office@kamsolutions.pl',
        link: "mailto:office@kamsolutions.pl",
        info: null,
    },
    {
        id: 3,
        icon: faTwitter,
        contactName: 'Twitter',
        link: "https://twitter.com/wordpress",
        info: 'https://twitter.com/wordpress',
    },
    {
        id: 4,
        icon: faFacebookF,
        contactName: 'Facebook',
        link: "https://www.facebook.com/facebook",
        info: "https://www.facebook.com/facebook",
    },
    {
        id: 5,
        icon: faSkype,
        contactName: 'Skype',
        link: "skype:kamsolutions.pl",
        info: 'kamsolutions.pl',
    }
]


const Contacts = () => {
    return (
        <div className="contacts">
            {contacts.map((contact)=>(
                <div className="contact" key={contact.id} data-testid="contact-item">
                    <div className="icon">
                        <FontAwesomeIcon icon={contact.icon} data-testid="contact-icon"/>
                    </div>
                    <div className="contact-data">
                        <a href={contact.link}>
                            <h4>{contact.contactName}</h4>
                            <p>{contact.info}</p>
                        </a>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Contacts
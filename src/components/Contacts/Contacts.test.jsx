import { render, screen } from '@testing-library/react';
import Contacts from './Contacts';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';


describe('Contacts component', () => {
  test('renders all contact items', () => {
    render(<Contacts />);
    const contactElements = screen.getAllByTestId('contact-item');
    expect(contactElements.length).toBe(5);
  });

  test('renders contact name and link correctly', () => {
    render(<Contacts />);
    const contactNameElements = screen.getAllByRole('heading', { level: 4 });
    const contactLinkElements = screen.getAllByRole('link');
    expect(contactNameElements.length).toBe(5);
    expect(contactLinkElements.length).toBe(5);
    expect(contactNameElements[0]).toHaveTextContent('500 342 242');
    expect(contactLinkElements[0]).toHaveAttribute('href', 'tel:500342342');
  });

  test('renders contact info correctly', () => {
    render(<Contacts />);
    const contactInfoElements = screen.getAllByRole('link');
    expect(contactInfoElements.length).toBe(5);
    expect(contactInfoElements[2]).toHaveTextContent('https://twitter.com/wordpress');
    expect(contactInfoElements[3]).toHaveTextContent('https://www.facebook.com/facebook');
    expect(contactInfoElements[4]).toHaveTextContent('kamsolutions.pl');
  });

  test('renders contact icons', () => {
    render(<Contacts />);
    const contactIconElements = screen.getAllByTestId('contact-icon');
    expect(contactIconElements.length).toBe(5);
  });
});

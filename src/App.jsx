import './App.scss'
import Home from './pages/Home/Home'
import Inner from './pages/Inner/Inner.jsx'

import { BrowserRouter, Routes, Route } from 'react-router-dom'

function App() {

  return (
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />}/>
          <Route path='/inner' element={<Inner/>}/>
        </Routes>
      </BrowserRouter>
  )
}

export default App


import guy from '../assets/guy.png'
import card1 from '../assets/Portfolio/card_1.png'
import card2 from '../assets/Portfolio/card_2.png'

export const text = 
'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque'

export const experience = 
    [
        {
            id: 1,
            date: '2013',
            company: 'Google',
            job: 'Front-end developer / php programmer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        },
        {
            id: 2,
            date: '2012', 
            company: 'Twitter',
            job: 'Web developer',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
        }
    ]

export const feedbacks = 
    [ 
        {
            id: 1,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            } 
        }, 
        {
            id: 2,
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 
            reporter: { 
                photoUrl: guy, 
                name: 'John Doe', 
                citeUrl: 'https://www.citeexample.com' 
            }
        } 
    ]

export const portfolioData = 
    [
        {
            id: 1,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 2,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 3,
            type: 'code',
            card: card1,
            title: 'Some thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
        {
            id: 4,
            type: 'ui',
            card: card2,
            title: 'Other thing',
            text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo',
            url: 'somesite.com'
        },
    ]

    
import { configureStore } from '@reduxjs/toolkit';

import timelineReducer from '../components/Timeline/timelineSlice';
import skillsReducer from '../components/Skills/skillsSlice';

export const store = configureStore({
  reducer: {
    timeline: timelineReducer,
    skills: skillsReducer
  },
});


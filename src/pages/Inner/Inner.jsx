import Box from "../../components/Box/Box"
import Panel from "../../components/Panel/Panel"
import Info from "../../components/Info/Info"
import './Inner.scss'
import Timeline from "../../components/Timeline/Timeline"
import Portfolio from "../../components/Portfolio/Portfolio"
import Experience from "../../components/Experience/Experience"
import Contacts from "../../components/Contacts/Contacts"
import Skills from "../../components/Skills/Skills"
import Feedbacks from "../../components/Feedbacks/Feedbacks"

import { HashLink } from "react-router-hash-link"
import { useState } from "react"
import { text, experience, feedbacks, portfolioData } from "../../utils/mockData"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faChevronUp } from "@fortawesome/free-solid-svg-icons"

const Inner = () => {
    const [navCollapse, setNavCollapse] = useState(false)
    return (
        <div className="inner-page" >
            <Panel navCollapse={navCollapse}/>
            
            <div className={`main-content ${navCollapse ? 'closed' : ''}`}>
                <button className={`hamburger ${navCollapse ? 'collapsed' : 'open'}`} onClick={()=>{setNavCollapse(!navCollapse)}}>
                        <span></span>
                        <span></span>
                        <span></span>
                </button>
                <section className="freespace" id='top'></section>
                <Box title='About Me' id='aboutme' content={<Info text={text}/>}/>
                <Box title='Education' id='education' content={<Timeline/>}/>
                <Box title='Experience' id='experience' content={<Experience data={experience}/>}/>
                <Box title='Skills' id='skills' content={<Skills/>}/>
                <Box title='Portfolio' id='portfolio' content={<Portfolio portfolioData={portfolioData}/>}/>
                <Box title='Contacts' id='contacts' content={<Contacts/>}/>
                <Box title='Feedbacks' id='feedbacks' content={<Feedbacks data={feedbacks}/>}/>
                <HashLink to='#top' smooth>
                    <button className="bottom">
                        <FontAwesomeIcon icon={faChevronUp}/>
                    </button> 
                </HashLink>
            </div>
        </div>
    )
}

export default Inner
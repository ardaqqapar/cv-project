import './Home.scss'
import Button from "../../components/Button/Button"
import PhotoBox from "../../components/PhotoBox/PhotoBox"

const Home = () => {
    return(
        <div className='home'>
            <PhotoBox name='Ardak' title='Front End Developer' description='Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque '/>
            <Button text='Know more' link='/inner'/>
        </div>
    )
}

export default Home
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    outDir: 'dist',
    assetsDir: 'assets',
    rollupOptions: {
      entry: 'assets/index-414e9362.js' // Replace 'index-414e9362.js' with the actual filename
    }
  } 
})
